using MediatR;
using TestApplication.Commands.PropertyToEssence.Response;

namespace TestApplication.Commands.PropertyToEssence.Request;

public class PatchPropertyValueToEssenceRequest : IRequest<PatchPropertyValueToEssenceResponse>
{
    /// <summary>
    /// ID значения свойства
    /// </summary>
    public int id { get; set; }
    
    /// <summary>
    /// Значение
    /// </summary>
    public string value { get; set; }
}