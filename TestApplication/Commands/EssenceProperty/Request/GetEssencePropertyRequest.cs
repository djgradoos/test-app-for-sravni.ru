using MediatR;
using TestApplication.Commands.EssenceProperty.Response;

namespace TestApplication.Commands.EssenceProperty.Request;

public class GetEssencePropertyRequest : IRequest<GetEssencePropertyResponse>
{
    /// <summary>
    /// ID свойства сущности (Если оставить пустым выдаст все записи)
    /// </summary>
    public int? id { get; set; }
}