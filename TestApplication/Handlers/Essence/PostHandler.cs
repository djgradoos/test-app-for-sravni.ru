using AutoMapper;
using MediatR;
using TestApplication.Commands.Essence.Request;
using TestApplication.Commands.Essence.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.Essence;

public class PostHandler : IRequestHandler<PostEssenceRequest, PostEssenceResponse>
{
    private readonly ILogger<PostHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IEssanceService _essance;
    
    public PostHandler(ILogger<PostHandler> logger, IMapper mapper, IEssanceService essance)
    {
        _logger = logger;
        _mapper = mapper;
        _essance = essance;
    }
    
    public async Task<PostEssenceResponse> Handle(PostEssenceRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _essance.PostItem(request, cancellationToken);
            return _mapper.Map<PostEssenceResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            return new PostEssenceResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}