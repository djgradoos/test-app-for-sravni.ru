﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TestApplication.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EssenceProperties",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    ValueType = table.Column<int>(type: "INTEGER", nullable: false),
                    IsDelete = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EssenceProperties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Essences",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Type = table.Column<int>(type: "INTEGER", nullable: false),
                    IsDelete = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Essences", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EssencePropertyValues",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    EssencePropertyId = table.Column<int>(type: "INTEGER", nullable: false),
                    IntValue = table.Column<int>(type: "INTEGER", nullable: false),
                    DoubleValue = table.Column<double>(type: "REAL", nullable: false),
                    StringValue = table.Column<string>(type: "TEXT", nullable: false),
                    BooleanValue = table.Column<bool>(type: "INTEGER", nullable: false),
                    DateTimeValue = table.Column<DateTime>(type: "TEXT", nullable: false),
                    IsDelete = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EssencePropertyValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EssencePropertyValues_EssenceProperties_EssencePropertyId",
                        column: x => x.EssencePropertyId,
                        principalTable: "EssenceProperties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PropertyToEssences",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    EssenceId = table.Column<int>(type: "INTEGER", nullable: false),
                    EssencePropertyId = table.Column<int>(type: "INTEGER", nullable: false),
                    EssencePropertyValueId = table.Column<int>(type: "INTEGER", nullable: false),
                    IsDelete = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyToEssences", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropertyToEssences_EssenceProperties_EssencePropertyId",
                        column: x => x.EssencePropertyId,
                        principalTable: "EssenceProperties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PropertyToEssences_EssencePropertyValues_EssencePropertyValueId",
                        column: x => x.EssencePropertyValueId,
                        principalTable: "EssencePropertyValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PropertyToEssences_Essences_EssenceId",
                        column: x => x.EssenceId,
                        principalTable: "Essences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EssencePropertyValues_EssencePropertyId",
                table: "EssencePropertyValues",
                column: "EssencePropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyToEssences_EssenceId",
                table: "PropertyToEssences",
                column: "EssenceId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyToEssences_EssencePropertyId",
                table: "PropertyToEssences",
                column: "EssencePropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyToEssences_EssencePropertyValueId",
                table: "PropertyToEssences",
                column: "EssencePropertyValueId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PropertyToEssences");

            migrationBuilder.DropTable(
                name: "EssencePropertyValues");

            migrationBuilder.DropTable(
                name: "Essences");

            migrationBuilder.DropTable(
                name: "EssenceProperties");
        }
    }
}
