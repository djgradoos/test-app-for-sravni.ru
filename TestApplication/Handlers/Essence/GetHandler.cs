using AutoMapper;
using MediatR;
using TestApplication.Commands.Essence.Request;
using TestApplication.Commands.Essence.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.Essence;

public class GetHandler : IRequestHandler<GetEssenceRequest, GetEssenceResponse>
{
    private readonly ILogger<GetHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IEssanceService _essance;
    
    public GetHandler(ILogger<GetHandler> logger, IMapper mapper, IEssanceService essance)
    {
        _logger = logger;
        _mapper = mapper;
        _essance = essance;
    }
    
    public async Task<GetEssenceResponse> Handle(GetEssenceRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _essance.GetItem(request.id, cancellationToken);
            return _mapper.Map<GetEssenceResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            return new GetEssenceResponse()
            {
                status = false,
                error = e.Message
            };
        }
        
    }
}