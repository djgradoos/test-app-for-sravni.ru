using TestApplication.Data.DTO.Base;
using TestApplication.Data.DTO.Response.EssencePropertyValue;
using TestApplication.Data.Enum;

namespace TestApplication.Data.DTO.Response.Essence;

public class EssenceDTO : BaseResponseDTO
{
    /// <summary>
    /// Тип сущности
    /// </summary>
    public EssenceType type { get; set; }

    /// <summary>
    /// Свойства и значения сущности
    /// </summary>
    public List<EssencePropertyValueDTO> propertyValues { get; set; }
}