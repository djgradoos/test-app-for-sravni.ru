using MediatR;
using TestApplication.Commands.Essence.Response;
using TestApplication.Data.Enum;

namespace TestApplication.Commands.Essence.Request;

public class PutEssenceRequest : IRequest<PutEssenceResponse>
{
    /// <summary>
    /// ID сущности
    /// </summary>
    public int id { get; set; }
    
    /// <summary>
    /// Имя сущности
    /// </summary>
    public string name { get; set; }
    
    /// <summary>
    /// Тип сущности
    /// </summary>
    public EssenceType type { get; set; }
}