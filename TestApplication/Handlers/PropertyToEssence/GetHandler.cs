using AutoMapper;
using MediatR;
using TestApplication.Commands.PropertyToEssence.Request;
using TestApplication.Commands.PropertyToEssence.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.PropertyToEssence;

public class GetHandler : IRequestHandler<GetPropertyToEssenceRequest, GetPropertyToEssenceResponse>
{
    private readonly ILogger<GetHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IPropertyToEssenceService _service;
    
    public GetHandler(ILogger<GetHandler> logger, IMapper mapper, IPropertyToEssenceService service)
    {
        _logger = logger;
        _mapper = mapper;
        _service = service;
    }
    
    public async Task<GetPropertyToEssenceResponse> Handle(GetPropertyToEssenceRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _service.GetItem(request.essenceId, cancellationToken);
            return _mapper.Map<GetPropertyToEssenceResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);

            return new GetPropertyToEssenceResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}