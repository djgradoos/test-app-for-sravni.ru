using AutoMapper;
using MediatR;
using TestApplication.Commands.EssenceProperty.Request;
using TestApplication.Commands.EssenceProperty.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.EssenceProperty;

public class PostHandler : IRequestHandler<PostEssencePropertyRequest, PostEssencePropertyResponse>
{
    private readonly ILogger<PostHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IEssancePropertyService _service;
    
    public PostHandler(ILogger<PostHandler> logger, IMapper mapper, IEssancePropertyService service)
    {
        _logger = logger;
        _mapper = mapper;
        _service = service;
    }
    
    public async Task<PostEssencePropertyResponse> Handle(PostEssencePropertyRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _service.PostItem(request, cancellationToken);
            return _mapper.Map<PostEssencePropertyResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            
            return new PostEssencePropertyResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}