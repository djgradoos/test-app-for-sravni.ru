using System.ComponentModel.DataAnnotations.Schema;
using TestApplication.Data.Entity.Base;

namespace TestApplication.Data.Entity;

/// <summary>
/// Свойство для сущности и его зачение
/// </summary>
public class PropertyToEssence : BaseEntity
{
    /// <summary>
    /// ID сущности
    /// </summary>
    public int EssenceId { get; set; }
    
    /// <summary>
    /// ID свойства
    /// </summary>
    public int EssencePropertyId { get; set; }
    
    /// <summary>
    /// ID значения свойства
    /// </summary>
    public int EssencePropertyValueId { get; set; }
    
    /// <summary>
    /// Сущность
    /// </summary>
    [ForeignKey("EssenceId")]
    public Essence Essence { get; set; }
    
    /// <summary>
    /// Свойство сущности
    /// </summary>
    [ForeignKey("EssencePropertyId")]
    public EssenceProperty Property { get; set; }

    /// <summary>
    /// Значение свойства сущности
    /// </summary>
    [ForeignKey("EssencePropertyValueId")]
    public EssencePropertyValue Value { get; set; }
}