using MediatR;
using TestApplication.Commands.Essence.Response;

namespace TestApplication.Commands.Essence.Request;

public class PatchEssenceNameRequest : IRequest<PatchEssenceNameResponse>
{
    /// <summary>
    /// ID сущности
    /// </summary>
    public int id { get; set; }
    
    /// <summary>
    /// Имя сущности
    /// </summary>
    public string name { get; set; }
}