using AutoMapper;
using MediatR;
using TestApplication.Commands.Essence.Request;
using TestApplication.Commands.Essence.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.Essence;

public class PatchNameHandler : IRequestHandler<PatchEssenceNameRequest, PatchEssenceNameResponse>
{
    private readonly ILogger<PatchNameHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IEssanceService _essance;
    
    public PatchNameHandler(ILogger<PatchNameHandler> logger, IMapper mapper, IEssanceService essance)
    {
        _logger = logger;
        _mapper = mapper;
        _essance = essance;
    }
    
    public async Task<PatchEssenceNameResponse> Handle(PatchEssenceNameRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _essance.PatchNameItem(request.id, request.name, cancellationToken);
            return _mapper.Map<PatchEssenceNameResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            return new PatchEssenceNameResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}