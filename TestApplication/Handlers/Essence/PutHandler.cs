using AutoMapper;
using MediatR;
using TestApplication.Commands.Essence.Request;
using TestApplication.Commands.Essence.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.Essence;

public class PutHandler : IRequestHandler<PutEssenceRequest, PutEssenceResponse>
{
    private readonly ILogger<PutHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IEssanceService _essance;
    
    public PutHandler(ILogger<PutHandler> logger, IMapper mapper, IEssanceService essance)
    {
        _logger = logger;
        _mapper = mapper;
        _essance = essance;
    }
    
    public async Task<PutEssenceResponse> Handle(PutEssenceRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _essance.PutItem(request, cancellationToken);
            return _mapper.Map<PutEssenceResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            return new PutEssenceResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}