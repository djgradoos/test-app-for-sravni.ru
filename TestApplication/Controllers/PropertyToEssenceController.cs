using MediatR;
using Microsoft.AspNetCore.Mvc;
using TestApplication.Commands.PropertyToEssence.Request;
using TestApplication.Commands.PropertyToEssence.Response;

namespace TestApplication.Controllers;

/// <summary>
/// Управление связью и значениями свойств сущностей
/// </summary>
[ApiController]
[Route("property/to/essence")]
public class PropertyToEssenceController : ControllerBase
{
    private readonly IMediator _mediator;

    public PropertyToEssenceController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    /// <summary>
    /// Получение значений свойств сущности
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>При переданном ID возвращает один экзепляр иначе все</returns>
    [HttpGet]
    public async Task<GetPropertyToEssenceResponse> Get([FromQuery]GetPropertyToEssenceRequest request, CancellationToken cancellationToken) =>
        await _mediator.Send(request, cancellationToken);
    
    /// <summary>
    /// Добавляет экземпляр значения свойства сущности
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<PostPropertyToEssenceResponse> Post([FromBody]PostPropertyToEssenceRequest request, CancellationToken cancellationToken) => 
        await _mediator.Send(request, cancellationToken);
    
    
    /// <summary>
    /// Обновляет значение экземпляра свойства сущности
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpPatch("value")]
    public async Task<PatchPropertyValueToEssenceResponse> Patch([FromBody]PatchPropertyValueToEssenceRequest request, CancellationToken cancellationToken) =>
        await _mediator.Send(request, cancellationToken);
    

    /// <summary>
    /// Помечает экземпляр значения свойства сущности как удаленный
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpDelete]
    public async Task<DeletePropertyToEssenceResponse> Delete([FromQuery]DeletePropertyToEssenceRequest request, CancellationToken cancellationToken) => 
        await _mediator.Send(request, cancellationToken);
}