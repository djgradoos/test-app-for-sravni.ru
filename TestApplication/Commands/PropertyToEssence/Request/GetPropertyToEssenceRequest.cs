using MediatR;
using TestApplication.Commands.PropertyToEssence.Response;

namespace TestApplication.Commands.PropertyToEssence.Request;

public class GetPropertyToEssenceRequest : IRequest<GetPropertyToEssenceResponse>
{
    /// <summary>
    /// ID сущности (Если оставить пустым выдаст все записи)
    /// </summary>
    public int? essenceId { get; set; }
}