using MediatR;
using TestApplication.Commands.Essence.Response;

namespace TestApplication.Commands.Essence.Request;

public class DeleteEssenceRequest : IRequest<DeleteEssenceResponse>
{
    /// <summary>
    /// ID сущности
    /// </summary>
    public int id { get; set; }
}