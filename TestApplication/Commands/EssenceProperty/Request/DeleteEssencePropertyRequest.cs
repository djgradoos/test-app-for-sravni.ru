using MediatR;
using TestApplication.Commands.EssenceProperty.Response;

namespace TestApplication.Commands.EssenceProperty.Request;

public class DeleteEssencePropertyRequest : IRequest<DeleteEssencePropertyResponse>
{
    /// <summary>
    /// ID свойства сущности
    /// </summary>
    public int id { get; set; }
}