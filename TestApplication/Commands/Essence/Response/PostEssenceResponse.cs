using TestApplication.Commands.Base.Response;

namespace TestApplication.Commands.Essence.Response;

public class PostEssenceResponse : BaseResponse
{
    public int id { get; set; }
}