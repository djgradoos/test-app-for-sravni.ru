using TestApplication.Commands.PropertyToEssence.Request;
using TestApplication.Data.DTO.Response.EssencePropertyValue;

namespace TestApplication.Intarfaces.Services;

public interface IPropertyToEssenceService
{
    /// <summary>
    /// Получение свойство сущности
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<List<EssencePropertyValueDTO>> GetItem(int? id, CancellationToken cancellationToken);
    
    /// <summary>
    /// Добавление свойства сущности
    /// </summary>
    /// <param name="data"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<int> PostItem(PostPropertyToEssenceRequest data, CancellationToken cancellationToken);
    
    /// <summary>
    /// Обновление всей сушности
    /// </summary>
    /// <param name="data"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<bool> PatchValueItem(PatchPropertyValueToEssenceRequest data, CancellationToken cancellationToken);
    
    /// <summary>
    /// Пометить сущность как удаленную
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<bool> DeleteItem(int id, CancellationToken cancellationToken);
}