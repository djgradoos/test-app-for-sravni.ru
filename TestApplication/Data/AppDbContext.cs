using Microsoft.EntityFrameworkCore;
using TestApplication.Data.Entity;

namespace TestApplication.Data;

public class AppDbContext : DbContext
{
    public DbSet<Essence> Essences { get; set; }
    public DbSet<EssenceProperty> EssenceProperties { get; set; }
    public DbSet<EssencePropertyValue> EssencePropertyValues { get; set; }
    public DbSet<PropertyToEssence> PropertyToEssences { get; set; }

    public AppDbContext(DbContextOptions options)
        : base(options)
    {
    }
}
