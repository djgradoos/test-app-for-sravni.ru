using MediatR;
using TestApplication.Commands.EssenceProperty.Response;
using TestApplication.Data.Enum;

namespace TestApplication.Commands.EssenceProperty.Request;

public class PutEssencePropertyRequest : IRequest<PutEssencePropertyResponse>
{
    /// <summary>
    /// ID свойства сущности
    /// </summary>
    public int id { get; set; }
    
    /// <summary>
    /// Имя свойства сущности
    /// </summary>
    public string name { get; set; }
    
    /// <summary>
    /// Тип свойства сущности
    /// </summary>
    public EssencePropertyValueType type { get; set; }
}