using AutoMapper;
using TestApplication.Commands.EssenceProperty.Request;
using TestApplication.Commands.EssenceProperty.Response;
using TestApplication.Data.DTO.Response.EssenceProperty;
using TestApplication.Data.Entity;

namespace TestApplication.Data.MappingProfiles;

public class EssencePropertyMappingProfile : Profile
{
    public EssencePropertyMappingProfile()
    {
        CreateMap<EssenceProperty, EssencePropertyDTO>()
            .ForMember(e => e.id, opt => opt.MapFrom(x => x.Id))
            .ForMember(e => e.name, opt => opt.MapFrom(x => x.Name))
            .ForMember(e => e.valueType, opt => opt.MapFrom(x => x.ValueType));

        CreateMap<PostEssencePropertyRequest, EssenceProperty>()
            .ForMember(e => e.Name, opt => opt.MapFrom(x => x.name))
            .ForMember(e => e.ValueType, opt => opt.MapFrom(x => x.type));
        
        CreateMap<PutEssencePropertyRequest, EssenceProperty>()
            .ForMember(e => e.Id, opt => opt.MapFrom(x => x.id))
            .ForMember(e => e.Name, opt => opt.MapFrom(x => x.name))
            .ForMember(e => e.ValueType, opt => opt.MapFrom(x => x.type));

        CreateMap<List<EssencePropertyDTO>, GetEssencePropertyResponse>()
            .ForMember(r => r.data, opt => opt.MapFrom(x => x));
        
        CreateMap<int, PostEssencePropertyResponse>()
            .ForMember(r => r.id, opt => opt.MapFrom(x => x));
        
        CreateMap<bool, PutEssencePropertyResponse>()
            .ForMember(r => r.status, opt => opt.MapFrom(x => x));
        
        CreateMap<bool, DeleteEssencePropertyResponse>()
            .ForMember(r => r.status, opt => opt.MapFrom(x => x));
    }
}