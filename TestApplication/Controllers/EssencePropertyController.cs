using MediatR;
using Microsoft.AspNetCore.Mvc;
using TestApplication.Commands.EssenceProperty.Request;
using TestApplication.Commands.EssenceProperty.Response;

namespace TestApplication.Controllers;

/// <summary>
/// Управление свойствами
/// </summary>
[ApiController]
[Route("essence/property")]
public class EssencePropertyController : ControllerBase
{
    private readonly IMediator _mediator;

    public EssencePropertyController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    /// <summary>
    /// Получение свойств
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>При переданном ID возвращает один экзепляр иначе все</returns>
    [HttpGet]
    public async Task<GetEssencePropertyResponse> Get([FromQuery]GetEssencePropertyRequest request, CancellationToken cancellationToken) => 
        await _mediator.Send(request, cancellationToken);
    
    /// <summary>
    /// Добавляет экземпляр свойства 
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<PostEssencePropertyResponse> Post([FromBody]PostEssencePropertyRequest request, CancellationToken cancellationToken) =>
        await _mediator.Send(request, cancellationToken);
    
    
    /// <summary>
    /// Обновляет экземпляр свойства 
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<PutEssencePropertyResponse> Put([FromBody]PutEssencePropertyRequest request, CancellationToken cancellationToken) => 
        await _mediator.Send(request, cancellationToken);
        
   
    /// <summary>
    /// Помечает экземпляр свойства как удаленный
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpDelete]
    public async Task<DeleteEssencePropertyResponse> Delete([FromQuery]DeleteEssencePropertyRequest request, CancellationToken cancellationToken) =>
        await _mediator.Send(request, cancellationToken);
    
}