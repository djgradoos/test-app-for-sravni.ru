namespace TestApplication.Data.Entity.Base;

public class BaseEntity
{
    /// <summary>
    /// ID Entity
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Статус удален/не удален
    /// </summary>
    public bool IsDelete { get; set; } = false;
}