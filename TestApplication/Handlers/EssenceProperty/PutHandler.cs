using AutoMapper;
using MediatR;
using TestApplication.Commands.EssenceProperty.Request;
using TestApplication.Commands.EssenceProperty.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.EssenceProperty;

public class PutHandler : IRequestHandler<PutEssencePropertyRequest, PutEssencePropertyResponse>
{
    private readonly ILogger<GetHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IEssancePropertyService _service;
    
    public PutHandler(ILogger<GetHandler> logger, IMapper mapper, IEssancePropertyService service)
    {
        _logger = logger;
        _mapper = mapper;
        _service = service;
    }
    
    public async Task<PutEssencePropertyResponse> Handle(PutEssencePropertyRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _service.PutItem(request, cancellationToken);
            return _mapper.Map<PutEssencePropertyResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            
            return new PutEssencePropertyResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}