using AutoMapper;
using MediatR;
using TestApplication.Commands.PropertyToEssence.Request;
using TestApplication.Commands.PropertyToEssence.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.PropertyToEssence;

public class PostHandler : IRequestHandler<PostPropertyToEssenceRequest, PostPropertyToEssenceResponse>
{
    private readonly ILogger<PostHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IPropertyToEssenceService _service;
    
    public PostHandler(ILogger<PostHandler> logger, IMapper mapper, IPropertyToEssenceService service)
    {
        _logger = logger;
        _mapper = mapper;
        _service = service;
    }
    
    public async Task<PostPropertyToEssenceResponse> Handle(PostPropertyToEssenceRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _service.PostItem(request, cancellationToken);
            return _mapper.Map<PostPropertyToEssenceResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            
            return new PostPropertyToEssenceResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}