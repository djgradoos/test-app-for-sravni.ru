using TestApplication.Commands.Base.Response;

namespace TestApplication.Commands.EssenceProperty.Response;

public class PostEssencePropertyResponse : BaseResponse
{
    public int id { get; set; }
}