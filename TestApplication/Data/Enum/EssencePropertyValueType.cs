namespace TestApplication.Data.Enum;

public enum EssencePropertyValueType
{
    Int = 0,
    Double = 1,
    String = 2,
    Boolean = 3,
    DateTime = 4
}