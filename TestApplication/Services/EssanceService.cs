using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TestApplication.Commands.Essence.Request;
using TestApplication.Data;
using TestApplication.Data.DTO.Response.Essence;
using TestApplication.Data.Entity;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Services;

public class EssanceService : IEssanceService
{
    private readonly AppDbContext _dbContext;
    private readonly IMapper _mapper;
    
    public EssanceService(AppDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }
    
    public async Task<List<EssenceDTO>> GetItem(int? Id, CancellationToken cancellationToken)
    {
        var queryable = _dbContext.Essences
            .AsNoTracking()
            .Where(x => !x.IsDelete);

        if (Id.HasValue)
        {
            queryable = queryable.Where(x => x.Id == Id);
        }

        var result = await queryable
            .Include(x => x.PropertyToEssence
                .Where(x => !x.IsDelete))
            .ThenInclude(x => x.Value)
            .ThenInclude(x => x.Property)
            .Include(x => x.PropertyToEssence
                .Where(x => !x.IsDelete))
            .ThenInclude(x => x.Property)
            .ToListAsync(cancellationToken);

        return _mapper.Map<List<EssenceDTO>>(result);
    }

    public async Task<int> PostItem(PostEssenceRequest data, CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(data.name)) throw new Exception("Name is requried parametr!");

        var model = _mapper.Map<Essence>(data);

        await _dbContext.Essences.AddAsync(model, cancellationToken);
        await _dbContext.SaveChangesAsync(cancellationToken);

        return model.Id;
    }

    public async Task<bool> PutItem(PutEssenceRequest data, CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(data.name)) throw new Exception("Name is requried parametr!");
 
        var modelCheck = await _dbContext.Essences
            .AsNoTracking()
            .Where(x => !x.IsDelete)
            .Where(x => x.Id == data.id)
            .AnyAsync(cancellationToken);

        if (!modelCheck) throw new Exception("Passed id does not refer to a database entry");

        _dbContext.Essences.Update(_mapper.Map<Essence>(data));

        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return true;
    }

    public async Task<bool> PatchNameItem(int id, string name, CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(name)) throw new Exception("Name is requried parametr!");
        
        var model = await _dbContext.Essences
            .Where(x => !x.IsDelete)
            .Where(x => x.Id == id)
            .FirstOrDefaultAsync(cancellationToken);

        if (model == null) throw new Exception("Passed id does not refer to a database entry");

        model.Name = name;

        _dbContext.Essences.Update(model);

        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return true;
    }

    public async Task<bool> DeleteItem(int id, CancellationToken cancellationToken)
    {
        var model = await _dbContext.Essences
            .Where(x => !x.IsDelete)
            .Where(x => x.Id == id)
            .FirstOrDefaultAsync(cancellationToken);

        if (model == null) throw new Exception("Passed id does not refer to a database entry");

        model.IsDelete = true;
        
        _dbContext.Essences.Update(model);

        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return true;
    }
}