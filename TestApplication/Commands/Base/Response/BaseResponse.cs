namespace TestApplication.Commands.Base.Response;

public class BaseResponse
{
    /// <summary>
    /// Статус выполнения запроса успех/провал
    /// </summary>
    public bool status { get; set; } = true;
    
    /// <summary>
    /// Поле ошибки наличие сообщения свидетельствует о наличии ошибки / в норме пустое.
    /// </summary>
    public string error { get; set; }
}