using TestApplication.Data.Entity.Base;
using TestApplication.Data.Enum;

namespace TestApplication.Data.Entity;

/// <summary>
/// Свойство сущности
/// </summary>
public class EssenceProperty : BaseEntity
{
    /// <summary>
    /// Название свойства сущности
    /// </summary>
    public string Name { get; set; }
    
    /// <summary>
    /// Тип данных свойства сущности
    /// </summary>
    public EssencePropertyValueType ValueType { get; set; }
}