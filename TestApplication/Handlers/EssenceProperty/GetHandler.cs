using AutoMapper;
using MediatR;
using TestApplication.Commands.EssenceProperty.Request;
using TestApplication.Commands.EssenceProperty.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.EssenceProperty;

public class GetHandler : IRequestHandler<GetEssencePropertyRequest, GetEssencePropertyResponse>
{
    private readonly ILogger<GetHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IEssancePropertyService _service;
    
    public GetHandler(ILogger<GetHandler> logger, IMapper mapper, IEssancePropertyService service)
    {
        _logger = logger;
        _mapper = mapper;
        _service = service;
    }
    
    public async Task<GetEssencePropertyResponse> Handle(GetEssencePropertyRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _service.GetItem(request.id, cancellationToken);
            return _mapper.Map<GetEssencePropertyResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            
            return new GetEssencePropertyResponse()
            {
                status = false,
                error = e.Message
            };
        }
        
    }
}