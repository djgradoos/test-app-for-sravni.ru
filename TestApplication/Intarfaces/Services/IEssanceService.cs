using TestApplication.Commands.Essence.Request;
using TestApplication.Data.DTO.Response.Essence;

namespace TestApplication.Intarfaces.Services;

public interface IEssanceService
{
    /// <summary>
    /// Получение сущностей всех или по ID
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<List<EssenceDTO>> GetItem(int? Id, CancellationToken cancellationToken);
    
    /// <summary>
    /// Добавление сущности
    /// </summary>
    /// <param name="data"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<int> PostItem(PostEssenceRequest data, CancellationToken cancellationToken);
    
    /// <summary>
    /// Обновление всей сушности
    /// </summary>
    /// <param name="data"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<bool> PutItem(PutEssenceRequest data, CancellationToken cancellationToken);
    
    /// <summary>
    /// Обновление имени сущности
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="name"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<bool> PatchNameItem(int Id, string name, CancellationToken cancellationToken);
    
    /// <summary>
    /// Пометить сущность как удаленную
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<bool> DeleteItem(int Id, CancellationToken cancellationToken);
}