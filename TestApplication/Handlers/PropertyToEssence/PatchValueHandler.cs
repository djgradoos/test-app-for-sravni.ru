using AutoMapper;
using MediatR;
using TestApplication.Commands.PropertyToEssence.Request;
using TestApplication.Commands.PropertyToEssence.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.PropertyToEssence;

public class PatchValueHandler : IRequestHandler<PatchPropertyValueToEssenceRequest, PatchPropertyValueToEssenceResponse>
{
    private readonly ILogger<PatchValueHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IPropertyToEssenceService _service;
    
    public PatchValueHandler(ILogger<PatchValueHandler> logger, IMapper mapper, IPropertyToEssenceService service)
    {
        _logger = logger;
        _mapper = mapper;
        _service = service;
    }
    
    public async Task<PatchPropertyValueToEssenceResponse> Handle(PatchPropertyValueToEssenceRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _service.PatchValueItem(request, cancellationToken);
            return _mapper.Map<PatchPropertyValueToEssenceResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            
            return new PatchPropertyValueToEssenceResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}