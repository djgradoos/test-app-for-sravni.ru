using AutoMapper;
using TestApplication.Commands.PropertyToEssence.Response;
using TestApplication.Data.DTO.Response.EssencePropertyValue;

namespace TestApplication.Data.MappingProfiles;

public class EssencePropertyValueMappingProfile : Profile
{
    public EssencePropertyValueMappingProfile()
    {
        CreateMap<List<EssencePropertyValueDTO>, GetPropertyToEssenceResponse>()
            .ForMember(r => r.data, opt => opt.MapFrom(x => x));

        CreateMap<int, PostPropertyToEssenceResponse>()
            .ForMember(r => r.id, opt => opt.MapFrom(x => x));

        CreateMap<bool, PatchPropertyValueToEssenceResponse>()
            .ForMember(r => r.status, opt => opt.MapFrom(x => x));

        CreateMap<bool, DeletePropertyToEssenceResponse>()
            .ForMember(r => r.status, opt => opt.MapFrom(x => x));
    }
}