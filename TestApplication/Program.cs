using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using TestApplication.Data;
using TestApplication.Data.MappingProfiles;
using TestApplication.Intarfaces.Services;
using TestApplication.Services;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseSqlite(connectionString));

builder.Services.AddControllers();

builder.Services.AddMediatR(typeof(Program));

builder.Services.AddAutoMapper(typeof(EssenceMappingProfile));
builder.Services.AddAutoMapper(typeof(EssencePropertyMappingProfile));
builder.Services.AddAutoMapper(typeof(EssencePropertyValueMappingProfile));
builder.Services.AddAutoMapper(typeof(PropertyToEssenceMappingProfile));

builder.Services.AddScoped<IEssanceService, EssanceService>();
builder.Services.AddScoped<IEssancePropertyService, EssancePropertyService>();
builder.Services.AddScoped<IPropertyToEssenceService, PropertyToEssenceService>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(opt =>
{
    opt.SwaggerDoc("v1",
        new OpenApiInfo()
        {
            Title = "Test App for sravni.ru",
            Version = "v1"
        }
    );
    var filePath = Path.Combine(AppContext.BaseDirectory, "TestApplication.xml");
    opt.IncludeXmlComments(filePath, true);
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();