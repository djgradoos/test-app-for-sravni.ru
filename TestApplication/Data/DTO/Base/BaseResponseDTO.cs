namespace TestApplication.Data.DTO.Base;

public class BaseResponseDTO
{
    /// <summary>
    /// ID
    /// </summary>
    public int id { get; set; }
    
    /// <summary>
    /// Название
    /// </summary>
    public string name { get; set; }
}