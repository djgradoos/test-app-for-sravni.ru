using TestApplication.Commands.Base.Response;
using TestApplication.Data.DTO.Response.Essence;
using TestApplication.Data.DTO.Response.EssencePropertyValue;

namespace TestApplication.Commands.PropertyToEssence.Response;

public class GetPropertyToEssenceResponse : BaseResponse
{
    public List<EssencePropertyValueDTO> data { get; set; }
}