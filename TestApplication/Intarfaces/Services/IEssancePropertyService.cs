using TestApplication.Commands.Essence.Request;
using TestApplication.Commands.EssenceProperty.Request;
using TestApplication.Data.DTO.Response.Essence;
using TestApplication.Data.DTO.Response.EssenceProperty;

namespace TestApplication.Intarfaces.Services;

public interface IEssancePropertyService
{
    /// <summary>
    /// Получение свойство сущности
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<List<EssencePropertyDTO>> GetItem(int? id, CancellationToken cancellationToken);
    
    /// <summary>
    /// Добавление свойства сущности
    /// </summary>
    /// <param name="data"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<int> PostItem(PostEssencePropertyRequest data, CancellationToken cancellationToken);
    
    /// <summary>
    /// Обновление всей сушности
    /// </summary>
    /// <param name="data"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<bool> PutItem(PutEssencePropertyRequest data, CancellationToken cancellationToken);
    
    /// <summary>
    /// Пометить сущность как удаленную
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<bool> DeleteItem(int id, CancellationToken cancellationToken);
}