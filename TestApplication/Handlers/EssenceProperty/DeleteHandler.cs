using AutoMapper;
using MediatR;
using TestApplication.Commands.EssenceProperty.Request;
using TestApplication.Commands.EssenceProperty.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.EssenceProperty;

public class DeleteHandler : IRequestHandler<DeleteEssencePropertyRequest, DeleteEssencePropertyResponse>
{
    private readonly ILogger<DeleteHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IEssancePropertyService _service;
    
    public DeleteHandler(ILogger<DeleteHandler> logger, IMapper mapper, IEssancePropertyService service)
    {
        _logger = logger;
        _mapper = mapper;
        _service = service;
    }
    
    public async Task<DeleteEssencePropertyResponse> Handle(DeleteEssencePropertyRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _service.DeleteItem(request.id, cancellationToken);
            return _mapper.Map<DeleteEssencePropertyResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            
            return new DeleteEssencePropertyResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}