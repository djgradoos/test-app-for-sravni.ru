using TestApplication.Data.DTO.Base;
using TestApplication.Data.Enum;

namespace TestApplication.Data.DTO.Response.EssencePropertyValue;

public class EssencePropertyValueDTO : BaseResponseDTO
{
    /// <summary>
    /// Тип данных свойства
    /// </summary>
    public EssencePropertyValueType valueType { get; set; }

    /// <summary>
    /// Значение свойства
    /// </summary>
    public dynamic value { get; set; }
    
    /// <summary>
    /// ID сущности
    /// </summary>
    public int essenceId { get; set; }
}