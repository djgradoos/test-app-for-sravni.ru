using System.Runtime.CompilerServices;
using TestApplication.Data.Entity;
using TestApplication.Data.Enum;

namespace TestApplication.Extensions;

public static class EssencePropertyExtension
{
    public static dynamic GetValue(this PropertyToEssence data)
    {
        switch (data.Property?.ValueType)
        {
            case EssencePropertyValueType.Int : return data.Value.IntValue;
            case EssencePropertyValueType.Double : return data.Value.DoubleValue;
            case EssencePropertyValueType.String : return data.Value.StringValue;
            case EssencePropertyValueType.Boolean : return data.Value.BooleanValue;
            case EssencePropertyValueType.DateTime : return data.Value.DateTimeValue;
            default : return "";
        }
    }

    public static void SetValue(this PropertyToEssence model, EssencePropertyValueType type, string value)
    {
        model.Value.SetValue(type, value);
    }
    
    public static void SetValue(this EssencePropertyValue model, EssencePropertyValueType type, string value)
    {
        switch (type)
        {
            case EssencePropertyValueType.Int:
                var valueInt = string.IsNullOrEmpty(value) ? 0 : Convert.ToInt32(value);
                model.IntValue = valueInt;
                break;
            case EssencePropertyValueType.Double:
                var valueDouble = string.IsNullOrEmpty(value) ? 0D : Convert.ToDouble(value);
                model.DoubleValue = valueDouble;
                break;
            case EssencePropertyValueType.String:
                var valueString = string.IsNullOrEmpty(value) ? "" : Convert.ToString(value);
                model.StringValue = valueString;
                break;
            case EssencePropertyValueType.Boolean:
                var valueBoolean = string.IsNullOrEmpty(value) ? false : Convert.ToBoolean(value);
                model.BooleanValue = valueBoolean;
                break;
            case EssencePropertyValueType.DateTime:
                var valueDateTime = Convert.ToDateTime(value);
                model.DateTimeValue = valueDateTime;
                break;
        }
    }
}