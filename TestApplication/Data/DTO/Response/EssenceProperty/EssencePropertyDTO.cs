using TestApplication.Data.DTO.Base;
using TestApplication.Data.Enum;

namespace TestApplication.Data.DTO.Response.EssenceProperty;

public class EssencePropertyDTO : BaseResponseDTO
{
    /// <summary>
    /// Тип данных свойства сущности
    /// </summary>
    public EssencePropertyValueType valueType { get; set; }
}