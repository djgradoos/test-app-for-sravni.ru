using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TestApplication.Commands.PropertyToEssence.Request;
using TestApplication.Data;
using TestApplication.Data.DTO.Response.EssencePropertyValue;
using TestApplication.Data.Entity;
using TestApplication.Extensions;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Services;

public class PropertyToEssenceService : IPropertyToEssenceService
{
    private readonly AppDbContext _dbContext;
    private readonly IMapper _mapper;
    
    public PropertyToEssenceService(AppDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }

    public async Task<List<EssencePropertyValueDTO>> GetItem(int? id, CancellationToken cancellationToken)
    {
        var query = _dbContext.PropertyToEssences
            .AsNoTracking()
            .Where(x => !x.IsDelete);

        if (id.HasValue)
        {
            query = query.Where(x => x.EssenceId == id);
        }

        var result = await query
            .Include(x => x.Essence)
            .Include(x => x.Property)
            .Include(x => x.Value)
            .ToListAsync(cancellationToken);

        return _mapper.Map<List<EssencePropertyValueDTO>>(result);
    }

    public async Task<int> PostItem(PostPropertyToEssenceRequest data, CancellationToken cancellationToken)
    {
        var model = _mapper.Map<PropertyToEssence>(data);
        
        var property = await _dbContext.EssenceProperties
            .AsNoTracking()
            .Where(x => !x.IsDelete)
            .FirstOrDefaultAsync(x => x.Id == model.EssencePropertyId);

        if (property != null)
        {
            model.Value = new EssencePropertyValue()
            {
                EssencePropertyId = data.propertyId
            };
            
            model.SetValue(property.ValueType, data.value);
        }
        
        await _dbContext.AddAsync(model, cancellationToken);
        await _dbContext.SaveChangesAsync(cancellationToken);

        return model.Id;
    }

    public async Task<bool> PatchValueItem(PatchPropertyValueToEssenceRequest data, CancellationToken cancellationToken)
    {
        var essencePropertyValueId = await _dbContext.PropertyToEssences
            .Where(x => !x.IsDelete)
            .Where(x => x.Id == data.id)
            .Select(x => x.EssencePropertyValueId)
            .FirstOrDefaultAsync(cancellationToken);

        if (essencePropertyValueId == null) throw new Exception("Property To Essences entity not fount.");
        
        var model = await _dbContext.EssencePropertyValues
            .Where(x => !x.IsDelete)
            .Where(x => x.Id == essencePropertyValueId)
            .Include(x => x.Property)
            .FirstOrDefaultAsync(cancellationToken);

        if (model == null) throw new Exception("Essence Property Values entity not fount.");
        
        model.SetValue(model.Property.ValueType, data.value);
        
        _dbContext.EssencePropertyValues.Update(model);
        
        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return true;
    }

    public async Task<bool> DeleteItem(int id, CancellationToken cancellationToken)
    {
        var model = await _dbContext.PropertyToEssences
            .Where(x => !x.IsDelete)
            .Where(x => x.Id == id)
            .Include(x => x.Value)
            .FirstOrDefaultAsync(cancellationToken);

        model.IsDelete = true;
        model.Value.IsDelete = true;

        _dbContext.Update(model);

        await _dbContext.SaveChangesAsync(cancellationToken);

        return true;
    }
}