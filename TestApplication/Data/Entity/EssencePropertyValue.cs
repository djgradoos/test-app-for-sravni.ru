using System.ComponentModel.DataAnnotations.Schema;
using TestApplication.Data.Entity.Base;

namespace TestApplication.Data.Entity;

/// <summary>
/// Значение свойства сущности
/// </summary>
public class EssencePropertyValue : BaseEntity
{
    /// <summary>
    /// ID Свойства сущности
    /// </summary>
    public int EssencePropertyId { get; set; }
    
    /// <summary>
    /// Свойство сущности
    /// </summary>
    [ForeignKey("EssencePropertyId")]
    public EssenceProperty Property { get; set; }

    /// <summary>
    /// Представление данных в виде int
    /// </summary>
    public int IntValue { get; set; } 
    
    /// <summary>
    /// Представление данных в виде double
    /// </summary>
    public double DoubleValue { get; set; }

    /// <summary>
    /// Представление данных в виде string
    /// </summary>
    public string StringValue { get; set; } = "";

    /// <summary>
    /// Представление данных в виде boolean
    /// </summary>
    public bool BooleanValue { get; set; }
    
    /// <summary>
    /// Представление данных в виде DateTime
    /// </summary>
    public DateTime DateTimeValue { get; set; }
}