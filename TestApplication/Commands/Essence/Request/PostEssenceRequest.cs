using MediatR;
using TestApplication.Commands.Essence.Response;
using TestApplication.Data.Enum;

namespace TestApplication.Commands.Essence.Request;

public class PostEssenceRequest : IRequest<PostEssenceResponse>
{
    /// <summary>
    /// Имя сущности
    /// </summary>
    public string name { get; set; }
    
    /// <summary>
    /// Тип сущности
    /// </summary>
    public EssenceType type { get; set; }
}