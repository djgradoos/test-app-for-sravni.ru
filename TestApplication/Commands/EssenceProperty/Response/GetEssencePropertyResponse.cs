using TestApplication.Commands.Base.Response;
using TestApplication.Data.DTO.Response.EssenceProperty;

namespace TestApplication.Commands.EssenceProperty.Response;

public class GetEssencePropertyResponse : BaseResponse
{
    public List<EssencePropertyDTO> data { get; set; }
}