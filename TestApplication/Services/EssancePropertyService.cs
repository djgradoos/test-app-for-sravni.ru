using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TestApplication.Commands.EssenceProperty.Request;
using TestApplication.Data;
using TestApplication.Data.DTO.Response.EssenceProperty;
using TestApplication.Data.Entity;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Services;

public class EssancePropertyService : IEssancePropertyService
{
    private readonly AppDbContext _dbContext;
    private readonly IMapper _mapper;
    
    public EssancePropertyService(AppDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }

    public async Task<List<EssencePropertyDTO>> GetItem(int? id, CancellationToken cancellationToken)
    {
        var queryable = _dbContext.EssenceProperties
            .AsNoTracking()
            .Where(x => !x.IsDelete);

        if (id.HasValue)
        {
            queryable = queryable.Where(x => x.Id == id);
        }

        var result = await queryable
            .ToListAsync(cancellationToken);

        return _mapper.Map<List<EssencePropertyDTO>>(result);
    }

    public async Task<int> PostItem(PostEssencePropertyRequest data, CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(data.name)) throw new Exception("Name is requried parametr!");

        var model = _mapper.Map<EssenceProperty>(data);

        await _dbContext.EssenceProperties.AddAsync(model, cancellationToken);
        await _dbContext.SaveChangesAsync(cancellationToken);

        var modelValue = new EssencePropertyValue()
        {
            EssencePropertyId = model.Id,
        };
        
        await _dbContext.EssencePropertyValues.AddAsync(modelValue, cancellationToken);
        await _dbContext.SaveChangesAsync(cancellationToken);

        return model.Id;
    }

    public async Task<bool> PutItem(PutEssencePropertyRequest data, CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(data.name)) throw new Exception("Name is requried parametr!");

        var modelCheck = await _dbContext.EssenceProperties
            .AsNoTracking()
            .Where(x => !x.IsDelete)
            .Where(x => x.Id == data.id)
            .AnyAsync(cancellationToken);

        if (!modelCheck) throw new Exception("Passed id does not refer to a database entry");

        _dbContext.EssenceProperties.Update(_mapper.Map<EssenceProperty>(data));

        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return true;
    }

    public async Task<bool> DeleteItem(int id, CancellationToken cancellationToken)
    {
        var model = await _dbContext.EssenceProperties
            .Where(x => !x.IsDelete)
            .FirstOrDefaultAsync(x => x.Id == id);

        if (model == null) throw new Exception("Passed id does not refer to a database entry");

        model.IsDelete = true;
        
        _dbContext.EssenceProperties.Update(model);

        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return true;
    }
}