using TestApplication.Commands.Base.Response;

namespace TestApplication.Commands.PropertyToEssence.Response;

public class PostPropertyToEssenceResponse : BaseResponse
{
    public int id { get; set; }
}