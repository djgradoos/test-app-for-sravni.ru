namespace TestApplication.Data.Enum;

public enum EssenceType
{
    Type_A = 0,
    Type_B = 1,
    Type_C = 2
}