using AutoMapper;
using MediatR;
using TestApplication.Commands.Essence.Request;
using TestApplication.Commands.Essence.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.Essence;

public class DeleteHandler : IRequestHandler<DeleteEssenceRequest, DeleteEssenceResponse>
{
    private readonly ILogger<DeleteHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IEssanceService _essance;
    
    public DeleteHandler(ILogger<DeleteHandler> logger, IMapper mapper, IEssanceService essance)
    {
        _logger = logger;
        _mapper = mapper;
        _essance = essance;
    }
    
    public async Task<DeleteEssenceResponse> Handle(DeleteEssenceRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _essance.DeleteItem(request.id, cancellationToken);
            return _mapper.Map<DeleteEssenceResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            return new DeleteEssenceResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}