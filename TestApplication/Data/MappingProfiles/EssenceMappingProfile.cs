using AutoMapper;
using TestApplication.Commands.Essence.Request;
using TestApplication.Commands.Essence.Response;
using TestApplication.Data.DTO.Response.Essence;
using TestApplication.Data.Entity;

namespace TestApplication.Data.MappingProfiles;

public class EssenceMappingProfile : Profile
{
    public EssenceMappingProfile()
    {
        CreateMap<Essence, EssenceDTO>()
            .ForMember(e => e.id, opt => opt.MapFrom(x => x.Id))
            .ForMember(e => e.name, opt => opt.MapFrom(x => x.Name))
            .ForMember(e => e.type, opt => opt.MapFrom(x => x.Type))
            .ForMember(e => e.propertyValues, opt => opt.MapFrom(x => x.PropertyToEssence));
        
        CreateMap<PostEssenceRequest, Essence>()
            .ForMember(e => e.Id, opt => opt.Ignore())
            .ForMember(e => e.Name, opt => opt.MapFrom(x => x.name))
            .ForMember(e => e.Type, opt => opt.MapFrom(x => x.type));
        
        CreateMap<PutEssenceRequest, Essence>()
            .ForMember(e => e.Id, opt => opt.MapFrom(x => x.id))
            .ForMember(e => e.Name, opt => opt.MapFrom(x => x.name))
            .ForMember(e => e.Type, opt => opt.MapFrom(x => x.type));
        
        CreateMap<List<EssenceDTO>, GetEssenceResponse>()
            .ForMember(r => r.data, opt => opt.MapFrom(x => x));
        
        CreateMap<int, PostEssenceResponse>()
            .ForMember(r => r.id, opt => opt.MapFrom(x => x));
        
        CreateMap<bool, PutEssenceResponse>()
            .ForMember(r => r.status, opt => opt.MapFrom(x => x));
        
        CreateMap<bool, PatchEssenceNameResponse>()
            .ForMember(r => r.status, opt => opt.MapFrom(x => x));
        
        CreateMap<bool, DeleteEssenceResponse>()
            .ForMember(r => r.status, opt => opt.MapFrom(x => x));
    }
}