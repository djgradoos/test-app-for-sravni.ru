using AutoMapper;
using MediatR;
using TestApplication.Commands.PropertyToEssence.Request;
using TestApplication.Commands.PropertyToEssence.Response;
using TestApplication.Intarfaces.Services;

namespace TestApplication.Handlers.PropertyToEssence;

public class DeleteHandler : IRequestHandler<DeletePropertyToEssenceRequest, DeletePropertyToEssenceResponse>
{
    private readonly ILogger<DeleteHandler> _logger;
    private readonly IMapper _mapper;
    private readonly IPropertyToEssenceService _service;
    
    public DeleteHandler(ILogger<DeleteHandler> logger, IMapper mapper, IPropertyToEssenceService service)
    {
        _logger = logger;
        _mapper = mapper;
        _service = service;
    }
    
    public async Task<DeletePropertyToEssenceResponse> Handle(DeletePropertyToEssenceRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await _service.DeleteItem(request.id, cancellationToken);
            return _mapper.Map<DeletePropertyToEssenceResponse>(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            
            return new DeletePropertyToEssenceResponse()
            {
                status = false,
                error = e.Message
            };
        }
    }
}