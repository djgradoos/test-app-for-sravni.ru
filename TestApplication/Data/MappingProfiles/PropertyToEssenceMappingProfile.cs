using AutoMapper;
using TestApplication.Commands.PropertyToEssence.Request;
using TestApplication.Data.DTO.Response.EssencePropertyValue;
using TestApplication.Data.Entity;
using TestApplication.Extensions;

namespace TestApplication.Data.MappingProfiles;

public class PropertyToEssenceMappingProfile : Profile
{
    public PropertyToEssenceMappingProfile()
    {
        CreateMap<PatchPropertyValueToEssenceRequest, PropertyToEssence>()
            .ForMember(e => e.Id, opt => opt.MapFrom(x => x.id));
        
        CreateMap<PostPropertyToEssenceRequest, PropertyToEssence>()
            .ForMember(e => e.EssenceId, opt => opt.MapFrom(x => x.essenceId))
            .ForMember(e => e.EssencePropertyId, opt => opt.MapFrom(x => x.propertyId))
            .ForMember(e => e.Value, opt => opt.MapFrom(x => 
                new EssencePropertyValue
                {
                    EssencePropertyId = x.propertyId
                }
            ));
        
        CreateMap<PropertyToEssence, EssencePropertyValueDTO>()
            .ForMember(p => p.id, opt => opt.MapFrom(x => x.Id))
            .ForMember(p => p.essenceId, opt => opt.MapFrom(x => x.EssenceId))
            .ForMember(p => p.name, opt => opt.MapFrom(x => x.Property.Name))
            .ForMember(p => p.value, opt => opt.MapFrom(x => x.GetValue()))
            .ForMember(p => p.valueType, opt => opt.MapFrom(x => x.Property.ValueType));
    }
}