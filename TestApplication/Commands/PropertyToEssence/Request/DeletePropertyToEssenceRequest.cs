using MediatR;
using TestApplication.Commands.PropertyToEssence.Response;

namespace TestApplication.Commands.PropertyToEssence.Request;

public class DeletePropertyToEssenceRequest : IRequest<DeletePropertyToEssenceResponse>
{
    /// <summary>
    /// ID записи
    /// </summary>
    public int id { get; set; }
}