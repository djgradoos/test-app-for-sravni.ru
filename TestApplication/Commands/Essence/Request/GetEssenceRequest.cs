using MediatR;
using TestApplication.Commands.Essence.Response;

namespace TestApplication.Commands.Essence.Request;

public class GetEssenceRequest : IRequest<GetEssenceResponse>
{
    /// <summary>
    /// ID сущности (Если оставить пустым выдаст все записи)
    /// </summary>
    public int? id { get; set; }
}