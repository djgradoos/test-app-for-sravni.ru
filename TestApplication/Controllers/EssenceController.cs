using MediatR;
using Microsoft.AspNetCore.Mvc;
using TestApplication.Commands.Essence.Request;
using TestApplication.Commands.Essence.Response;

namespace TestApplication.Controllers;

/// <summary>
/// Управление сущностями
/// </summary>
[ApiController]
[Route("essence")]
public class EssenceController : ControllerBase
{
    private readonly IMediator _mediator;

    public EssenceController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Получение сущностей их свойств и значений
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>При переданном ID возвращает один экзепляр иначе все</returns>
    [HttpGet]
    public async Task<GetEssenceResponse> Get([FromQuery] GetEssenceRequest request, CancellationToken cancellationToken) =>
        await _mediator.Send(request, cancellationToken);
    
    /// <summary>
    /// Добавляет экземпляр сущности
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<PostEssenceResponse> Post([FromBody]PostEssenceRequest request, CancellationToken cancellationToken) => 
        await _mediator.Send(request, cancellationToken);
    
    
    /// <summary>
    /// Обновляет экземпляр сущности
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<PutEssenceResponse> Put([FromBody]PutEssenceRequest request, CancellationToken cancellationToken) => 
        await _mediator.Send(request, cancellationToken);
            
    /// <summary>
    /// Обновляет имя экземпляра сущности
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpPatch("name")]
    public async Task<PatchEssenceNameResponse> Patch([FromBody]PatchEssenceNameRequest request, CancellationToken cancellationToken) => 
        await _mediator.Send(request, cancellationToken);
            
    /// <summary>
    /// Помечает сущность как удаленную
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    [HttpDelete]
    public async Task<DeleteEssenceResponse> Delete([FromQuery]DeleteEssenceRequest request, CancellationToken cancellationToken) => 
        await _mediator.Send(request, cancellationToken);
}