using TestApplication.Data.Entity.Base;
using TestApplication.Data.Enum;

namespace TestApplication.Data.Entity;

/// <summary>
/// Сущность
/// </summary>
public class Essence : BaseEntity
{
    /// <summary>
    /// Название сущности
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Тип сущности
    /// </summary>
    public EssenceType Type { get; set; }

    /// <summary>
    /// Свойства и значения сущности
    /// </summary>
    public IEnumerable<PropertyToEssence> PropertyToEssence { get; set; }
}