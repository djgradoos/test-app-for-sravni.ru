using TestApplication.Commands.Base.Response;
using TestApplication.Data.DTO.Response.Essence;

namespace TestApplication.Commands.Essence.Response;

public class GetEssenceResponse : BaseResponse
{
    public List<EssenceDTO> data { get; set; }
}