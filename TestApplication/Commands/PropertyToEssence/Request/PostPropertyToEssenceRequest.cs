using MediatR;
using TestApplication.Commands.PropertyToEssence.Response;
using TestApplication.Data.Enum;

namespace TestApplication.Commands.PropertyToEssence.Request;

public class PostPropertyToEssenceRequest : IRequest<PostPropertyToEssenceResponse>
{
    /// <summary>
    /// ID сущности
    /// </summary>
    public int essenceId { get; set; }
    
    /// <summary>
    /// ID свойства
    /// </summary>
    public int propertyId { get; set; }

    /// <summary>
    /// Значение свойства
    /// </summary>
    public string value { get; set; }
}